<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
--> 
<iso:schema
        xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:iso="http://purl.oclc.org/dsdl/schematron"
        queryBinding='xslt2'
        schemaVersion='ISO19757-3'>

    <iso:title>Validation for GetCarePlans</iso:title>
    <iso:ns prefix='urn1' uri='urn:riv:clinicalprocess:logistics:logistics:3'/>
    <iso:ns prefix='urn' uri='urn:riv:clinicalprocess:logistics:logistics:GetCarePlansResponder:2'/>
	<iso:ns prefix='soapenv' uri='http://schemas.xmlsoap.org/soap/envelope/'/>

	<iso:pattern abstract="true" id="pattern.CvTypeStrict">		
		<iso:rule context="$path/urn1:code">
			<iso:assert test="count(../urn1:code) = 1">
				In $path code must be given.</iso:assert>
			<iso:assert test="count(../urn1:codeSystem) = 1">
				In $path codeSystem must be given.</iso:assert>
			<iso:assert test="count(../urn1:displayName) = 1">
				In $path displayName must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path in originalText must not be given.</iso:assert>
		</iso:rule>
	</iso:pattern>
	
	<iso:pattern abstract="true" id="pattern.IITypeStrict">		
		<iso:rule context="$path/urn1:code">
			<iso:assert test="count(../urn1:extension) = 1">
				In $path extension must be given.</iso:assert>
		</iso:rule>
	</iso:pattern>

	<iso:pattern abstract="true" id="pattern.CvType">
		<iso:rule context="$path">
			<iso:assert test="count(urn1:code) + count(urn1:originalText) = 1">
				In $path, either code or originalText must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:code">
			<iso:assert test="count(../urn1:codeSystem) + count(../urn1:displayName) = 2">
				In $path if <iso:name/> is given, codeSystem and displayName must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:codeSystem">
			<iso:assert test="count(../urn1:code) + count(../urn1:displayName) = 2">
				In $path if <iso:name/> is given, code and displayName must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
			<iso:assert test="matches(.,'^[0-2](\.([0-9])+)+$')">
                Element 'codeSystem' must be an OID for the current codeSystem.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:displayName">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) = 2">
				In $path if <iso:name/> is given, code and codeSystem must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:originalText">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) + count(../urn1:displayName) = 0">
				In $path if <iso:name/> is given, code, codeSystem and displayName must not be given.</iso:assert>
		</iso:rule>
	</iso:pattern>
	
	<iso:pattern id="Verify healthcareProfessionalRoleCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:healthcareProfessionalRoleCode" />
	</iso:pattern>

	<!-- Rules for Response, fields that MUST NOT exist in the response -->
	<iso:pattern id="No nullified">
		<iso:rule context="urn1:nullified">
			<iso:assert test="not(exists(.))"><iso:name/> MUST NOT exist in urn1:carePlanHeader</iso:assert>
		</iso:rule>
	</iso:pattern>

	<iso:pattern id="No content.id">
		<iso:rule context="urn1:content/urn1:id">
			<iso:assert test="not(exists(.))"><iso:name/> MUST NOT exist in urn1:carePlanHeader</iso:assert>
		</iso:rule>
	</iso:pattern>

	<iso:pattern id="No nullifiedReason">
		<iso:rule context="urn1:nullifiedReason">
			<iso:assert test="not(exists(.))"><iso:name/> MUST NOT exist in urn1:carePlanHeader</iso:assert>
		</iso:rule>
	</iso:pattern>

	<!-- Choice rules for Response, fields that MUST exist given some precondition -->
	<iso:pattern id="documentTitle">
		<iso:rule context="urn1:carePlanHeader">
			<iso:assert test="count(urn1:documentTitle) = 1"><iso:name/> MUST exist in urn1:carePlanHeader</iso:assert>
		</iso:rule>
	</iso:pattern>

	<!-- Verify that only one or none of carePlanBody/content/value and reference exists  -->
	<iso:pattern id="Verify that only one or none of reference and value exists">
		<iso:rule context="urn1:carePlanBody/urn1:content">
			<iso:assert test="count(urn1:value) + count(urn1:reference) = 0 
				or count(urn1:value) + count(urn1:reference) = 1">
				If reference is given, value must NOT be given and vice versa.</iso:assert>
		</iso:rule>
	</iso:pattern>

	<!-- Verify that only allowed media types is used  -->
	<iso:pattern id="Verify mediaType">
		<iso:rule context="urn:carePlan/urn1:carePlanBody/urn1:content">
			<iso:assert test="urn1:mediaType = 'text/plain' or
							  urn1:mediaType = 'text/html' or
							  urn1:mediaType = 'image/png' or
							  urn1:mediaType = 'image/jpeg' or
							  urn1:mediaType = 'image/tiff' or
							  urn1:mediaType = 'application/pdf'">
				Only allowed values in element carePlan/carePlanBody/content/mediaType are "text/plain", "text/html", "image/png", "image/jpeg", "image/tiff" and "application/pdf".
			</iso:assert>
		</iso:rule>
	</iso:pattern>
	
	<iso:pattern id="Verify non-empty elements">
        <iso:rule context="soapenv:Body/urn:GetCarePlansResponse//*">
		    <iso:assert test="normalize-space(.)">Element <iso:name /> is included but empty. All elements included in the response must have valid values.</iso:assert>
        </iso:rule>
    </iso:pattern>
	
	<iso:pattern id="Verify unique recordId">
		<iso:rule context="urn:GetCarePlansResponse">
			<let name="recordIdPath" value="urn:carePlan/urn1:carePlanHeader/urn1:documentId"/>
			<iso:assert test="count($recordIdPath) = count(distinct-values($recordIdPath))">Each record must have a unique documentId.</iso:assert>
		</iso:rule>
	</iso:pattern>
	
</iso:schema>
