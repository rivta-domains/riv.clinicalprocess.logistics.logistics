<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
-->
<iso:schema
        xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:iso="http://purl.oclc.org/dsdl/schematron"
        queryBinding='xslt2'
        schemaVersion='ISO19757-3'>

    <iso:title>Validation for GetCareContacts</iso:title>
    <iso:ns prefix='urn1' uri='urn:riv:clinicalprocess:logistics:logistics:3'/>
    <iso:ns prefix='urn' uri='urn:riv:clinicalprocess:logistics:logistics:GetCareContactsResponder:3'/>
	<iso:ns prefix='soapenv' uri='http://schemas.xmlsoap.org/soap/envelope/'/>

	<iso:pattern abstract="true" id="pattern.CvType">
		<iso:rule context="$path">
			<iso:assert test="count(urn1:code) + count(urn1:originalText) = 1">
				In $path, either code or originalText must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:code">
			<iso:assert test="count(../urn1:codeSystem) + count(../urn1:displayName) = 2">
				In $path if <iso:name/> is given, codeSystem and displayName must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:codeSystem">
			<iso:assert test="count(../urn1:code) + count(../urn1:displayName) = 2">
				In $path if <iso:name/> is given, code and displayName must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:displayName">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) = 2">
				In $path if <iso:name/> is given, code and codeSystem must be given.</iso:assert>
			<iso:assert test="count(../urn1:originalText)= 0">
				In $path if <iso:name/> is given, originalText must not be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:originalText">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) + count(../urn1:displayName) = 0">
				In $path if <iso:name/> is given, code, codeSystem and displayName must not be given.</iso:assert>
		</iso:rule>
	</iso:pattern>

    <!-- Implementing abstract patterns -->
	
	<iso:pattern id="Verify healthcareProfessionalRoleCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:healthcareProfessionalRoleCode" />
    </iso:pattern>
	<iso:pattern id="Verify careContactCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:careContactCode" />
    </iso:pattern>
	<iso:pattern id="Verify careContactStatus" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:careContactStatus" />
    </iso:pattern>
	<iso:pattern id="Verify gender" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:gender" />
    </iso:pattern>

	<!-- Rules for elements -->
	
    <iso:pattern id="Verify dateOfBirth">
        <iso:rule context="urn1:dateOfBirth">
            <iso:assert test="string-length(urn1:value) = string-length(urn1:format)"><iso:name /> must have matching format and value elements.</iso:assert>
        </iso:rule>
    </iso:pattern>
	
	<iso:pattern id="Verify resultCode">
        <iso:rule context="urn:result">
            <assert test="(urn1:resultCode = 'ERROR' and urn1:errorCode) or urn1:resultCode != 'ERROR'">If resultCode is ERROR, errorCode must be given.</assert>
        </iso:rule>
    </iso:pattern>
	
	<!-- Fields that MUST NOT exist -->
	<iso:pattern id="No forbidden elements">
		<iso:rule context="urn1:careContactHeader">
			<iso:assert test="not(urn1:documentTitle)">urn1:documentTitle MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:documentTime)">urn1:documentTime MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:legalAuthenticator)">urn1:legalAuthenticator MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:nullified)">urn1:nullified MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:nullifiedReason)">urn1:nullifiedReason MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:careContactId)">urn1:careContactId MUST NOT exist in <iso:name/></iso:assert>
		</iso:rule>
		<iso:rule context="urn1:careContactBody/urn1:additionalPatientInformation/urn1:gender">
			<iso:assert test="not(urn1:originalText)">urn1:originalText MUST NOT exist in urn1:additionalPatientInformation/<iso:name/></iso:assert>
		</iso:rule>
	</iso:pattern>


    <!-- Other rules -->
    <iso:pattern id="Verify non-empty elements">
        <iso:rule context="soapenv:Body/urn:GetCareContactsResponse//*">
            <iso:assert test="normalize-space(.)">Element <iso:name /> is included but empty. All elements included in the response must have valid values.</iso:assert>
        </iso:rule>
    </iso:pattern>
	
    <iso:pattern id="Verify unique id:s">
        <iso:rule context="urn:GetCareContactsResponse">
            <let name="recordIdPath" value="urn:careContact/urn1:careContactHeader//urn1:documentId"/>
            <iso:assert test="count($recordIdPath) = count(distinct-values($recordIdPath))">Each record must have a unique id.</iso:assert>
        </iso:rule>
    </iso:pattern>	
</iso:schema>
